//
// Created by cokafor3 on 3/15/2016.
//

#include <fstream>
#include <iostream>
#include <iomanip>
#include <CardCollection.h>
#include <sstream>
#include <stdlib.h>
#include <limits>
#include "ConditionConventer.h"
#include "CardFileIO.h"

CardFileIO::CardFileIO() {
}

CardFileIO::~CardFileIO() {
}

bool CardFileIO::checkForCards(string fileName) {
    ifstream file(fileName);
    if (file.peek() == ifstream::traits_type::eof()) {
        cout << "There are not currently any input." << endl << endl;
        return false;
    } else {
        cout << "File loaded/saved successfully." << endl << endl;
        return true;
    }
}

string CardFileIO::getFile() {
    string fileName;
    cin >> fileName;
    cout << endl;
    return fileName;
}

void CardFileIO::getCardName(Card *card) {
    cout << "Enter the player's name: ";
    string name = card->getName();
    bool valid = false;
    do {
        cin.clear();
        cin.sync();
        cin.ignore();
        getline(cin, name);
        cout << endl;
        if (cin.good()) {
            card->setName(name);
            valid = true;
        } else {
            cin.clear();
            cin.ignore(numeric_limits<streamsize>::max(), '\n');
            cerr << "Invalid name, please enter again." << endl;
        }
    } while (!valid);
}

void CardFileIO::getCardYear(Card *card) {
    cout << "Enter the year: ";
    stringstream out;
    int year = card->getYear();
    out << year;
    string tempYear = out.str();
    cin >> tempYear;
    cout << endl;
    year = atoi(tempYear.c_str());
    card->setYear(year);
}

void CardFileIO::getCardCondition(Card *card) {
    cout << "Enter the condition: ";
    string condition = ConditionConverter::CovertCondition(card->getCondition());
    cin >> condition;
    cout << endl;
    card->setCondition(ConditionConverter::CovertString(condition));
}

void CardFileIO::getCardValue(Card *card) {
    cout << "Enter the value: ";
    stringstream out;
    int value = card->getValue();
    out << value;
    string tempValue = out.str();
    cin >> tempValue;
    cout << endl;
    value = atoi(tempValue.c_str());
    card->setValue(value);
}

void CardFileIO::load() {
    cout << "Enter the file to load: ";

    string fileName = getFile();
    ifstream infile(fileName);
    string line;

    if (checkForCards(fileName)) {
        while (getline(infile, line)) {
            istringstream iss(line);

            Card *myEntry = new Card();

            loadCardName(iss, myEntry);

            loadCardYear(iss, myEntry);

            loadCardCondition(iss, myEntry);

            loadCardValue(iss, myEntry);

            this->cards.InsertCardByName(myEntry);
            this->cards.InsertCardByYear(myEntry);
            this->cards.InsertCardByCondition(myEntry);
        }
    }
}

void CardFileIO::loadCardName(istringstream &iss, Card *myEntry) const {
    string name = myEntry->getName();
    getline(iss, name, ',');
    myEntry->setName(name);
}

void CardFileIO::loadCardYear(istringstream &iss, Card *myEntry) const {
    stringstream out;
    int year = myEntry->getYear();
    out << year;
    string tempYear = out.str();
    getline(iss, tempYear, ',');
    year = atoi(tempYear.c_str());
    myEntry->setYear(year);
}

void CardFileIO::loadCardCondition(istringstream &iss, Card *myEntry) const {
    string condition = ConditionConverter::CovertCondition(myEntry->getCondition());
    getline(iss, condition, ',');
    myEntry->setCondition(ConditionConverter::CovertString(condition));
}

void CardFileIO::loadCardValue(istringstream &iss, Card *myEntry) const {
    stringstream out;
    int value = myEntry->getValue();
    out << value;
    string tempValue = out.str();
    getline(iss, tempValue, ',');
    value = atoi(tempValue.c_str());
    myEntry->setValue(value);
}

void CardFileIO::save() {
    cout << "Enter the file to save to: ";

    string filename = getFile();

    if (!checkForCards(filename)) {
        this->cards.outputCardsFromList(filename);
    } else {
        this->cards.outputCardsFromList(filename);
        cout << "The file has been overwritten!" << endl;
        cout << endl;
    }
}

void CardFileIO::deleteCard() {
    cout << "Enter the player's name: ";
    string playerName;
    cin >> playerName;
    this->cards.DeleteCardByName(playerName);
}

void CardFileIO::InsertCard() {
    Card *myEntry = new Card();

    this->getCardName(myEntry);
    this->getCardYear(myEntry);
    this->getCardCondition(myEntry);
    this->getCardValue(myEntry);

    this->cards.InsertCardByName(myEntry);
    this->cards.InsertCardByYear(myEntry);
    this->cards.InsertCardByCondition(myEntry);
}

void CardFileIO::printListAlphabeticalAscend() {
    this->cards.printList(CardLaces::NAME, PrintDirections::ASCENDING);
}

void CardFileIO::printListAlphabeticalDescend() {
    this->cards.printList(CardLaces::NAME, PrintDirections::DESCENDING);
}

void CardFileIO::printChronologicalAscend() {
    this->cards.printList(CardLaces::YEAR, PrintDirections::ASCENDING);
}

void CardFileIO::printChronologicalDescend() {
    this->cards.printList(CardLaces::YEAR, PrintDirections::DESCENDING);
}

void CardFileIO::printCondAscend() {
    this->cards.printList(CardLaces::CONDITION, PrintDirections::ASCENDING);
}

void CardFileIO::printCondDescend() {
    this->cards.printList(CardLaces::CONDITION, PrintDirections::DESCENDING);
}
