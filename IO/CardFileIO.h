//
// Created by cokafor3 on 3/15/2016.
//

#ifndef CARDINTERLACER_CARDFILELOADER_H
#define CARDINTERLACER_CARDFILELOADER_H
#include <string>
#include <vector>
#include "Card.h"
#include <CardCollection.h>
#include <CardView.h>

using namespace std;


class CardFileIO {
private:
    CardCollection cards;
    bool checkForCards(string fileName);
    void getCardName(Card * card);
    void getCardYear(Card * card);
    void getCardValue(Card * card);
    void getCardCondition(Card * card);
    void loadCardYear(istringstream &iss, Card *myEntry) const;
    void loadCardValue(istringstream &iss, Card *myEntry) const;
    void loadCardName(istringstream &iss, Card *myEntry) const;

public:
    CardFileIO();
    virtual ~CardFileIO();
    void load();
    void save();
    void deleteCard();
    void InsertCard();
    void printListAlphabeticalAscend();
    void printListAlphabeticalDescend();
    void printChronologicalAscend();
    void printChronologicalDescend();
    void printCondAscend();
    void printCondDescend();
    string getFile();


    void loadCardCondition(istringstream &iss, Card *myEntry) const;
};


#endif //CARDINTERLACER_CARDFILELOADER_H
