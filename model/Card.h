//
// Created by cokafor3 on 3/15/2016.
//

#ifndef CARDINTERLACER_CARDNODE_H
#define CARDINTERLACER_CARDNODE_H

#include <string>
#include "Conditions.h"

using namespace std;
namespace model {

    /*
     * Card Node class.
     */
    class Card {
    private:

        Card *nextName;
        Card *nextYear;
        Card *nextCondition;
        string name;
        int value;
        int year;
        Conditions condition;
    public:

        /*
         * Default Constructor
         */
        Card();

        /*
         * Overload Constructor
         */
        Card(const string &name, const int &year, const Conditions &condition, const int &value);

        /*
         * Deconstructor
         */
        virtual ~Card();

        /*
         * Card players name getter
         */
        string getName();

        /*
         * Card players name setter
         */
        void setName(const string &name);

        /*
         * Card players next name getter
         */
        Card *getNextName() const;

        /*
         * Card players next name setter
         */
        void setNextName(Card *nextName);

        /*
         * Card players year getter
         */
        int getYear();

        /*
         * Card players year setter
         */
        void setYear(const int &year);

        /*
         * Card players next year getter
         */
        Card *getNextYear() const;

        /*
         * Card players next year setter
         */
        void setNextYear(Card *nextYear);

        /*
         * Card players value getter
         */
        int getValue();

        /*
         * Card players value setter
         */
        void setValue(const int &value);

        /*
         * Card players condition getter
         */
        Conditions getCondition();

        /*
         * Card players condition setter
         */
        void setCondition(const Conditions &conditionValue);

        /*
         * Card players next condition getter
         */
        Card *getNextCondition() const;

        /*
         * Card players next condition setter
         */
        void setNextCondition(Card *nextCondition);
    };
}


#endif //CARDINTERLACER_CARDNODE_H
