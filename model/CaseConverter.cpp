//
// Created by cokafor3 on 3/28/2016.
//

#include <string.h>
#include "CaseConverter.h"

namespace model {
    bool CaseConverter::equal_case_insensitive(const string &s1, const string &s2) {
        return(0 == strcasecmp(s1.c_str(), s2.c_str()));
    }
}

