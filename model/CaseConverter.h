//
// Created by cokafor3 on 3/28/2016.
//

#ifndef CARDINTERLACER_COVERTCASE_H
#define CARDINTERLACER_COVERTCASE_H

#include <string>

using namespace std;

namespace model {
    class CaseConverter {
    public:
        static bool equal_case_insensitive(const string& s1, const string& s2);
    };
}



#endif //CARDINTERLACER_COVERTCASE_H
