//
// Created by cokafor3 on 3/26/2016.
//

#ifndef CARDINTERLACER_PRINTDIRECTIONS_H
#define CARDINTERLACER_PRINTDIRECTIONS_H

namespace model {

    /*
     * Print direction enum class
     */
    enum class PrintDirections {
        ASCENDING,
        DESCENDING
    };
}



#endif //CARDINTERLACER_PRINTDIRECTIONS_H
