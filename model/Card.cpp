//
// Created by cokafor3 on 3/15/2016.
//

#include <iostream>
#include "Card.h"

namespace model {
    Card::Card() {
        this->nextName = 0;
        this->nextYear = 0;
        this->nextCondition = 0;
        this->name = "";
        this->year = 0;
        this->condition = Conditions::EXCELLENT;
        this->value = 0;
    }

    Card::Card(const string &name, const int &year, const Conditions &condition, const int &value) {
        this->name = name;
        this->nextName = 0;

        this->year = year;
        this->nextYear = 0;

        this->condition = condition;
        this->nextCondition = 0;

        this->value = value;
    }

    Card::~Card() {
        this->nextName = 0;
        this->nextYear = 0;
        this->nextCondition = 0;
    }

    string Card::getName() {
        return this->name;
    }

    Card *Card::getNextName() const {
        return this->nextName;
    }


    void Card::setName(const string &name) {
        this->name = name;
    }

    void Card::setNextName(Card *nextName) {
        this->nextName = nextName;
    }

    int Card::getYear() {
        return this->year;
    }

    void Card::setYear(const int &year) {
        this->year = year;
    }

    Card *Card::getNextYear() const {
        return this->nextYear;
    }

    void Card::setNextYear(Card *nextYear) {
        this->nextYear = nextYear;
    }

    int Card::getValue() {
        return this->value;
    }

    void Card::setValue(const int &value) {
        this->value = value;
    }

    Conditions Card::getCondition() {
        return this->condition;
    }

    void Card::setCondition(const Conditions &conditionValue) {
        this->condition = conditionValue;
    }


    Card *Card::getNextCondition() const {
        return this->nextCondition;
    }

    void Card::setNextCondition(Card *nextCondition) {
        this->nextCondition = nextCondition;
    }
}
