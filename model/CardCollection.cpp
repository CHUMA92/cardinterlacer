//
// Created by cokafor3 on 3/17/2016.
//

#include <iostream>
#include <iomanip>
#include <sstream>
#include <fstream>
#include "CardCollection.h"
#include "ConditionConventer.h"
#include "CaseConverter.h"

namespace model {
    CardCollection::CardCollection() {
    }

    CardCollection::~CardCollection() {
    }

    void CardCollection::InsertCardByName(Card *newCard) {
        if (this->headName == 0) {
            this->headName = newCard;
        } else {
            insertNameAtOrAfterHead(newCard);

        }
        this->size++;
    }

    void CardCollection::insertNameAtOrAfterHead(Card *newCard) {
        Card *currentCard;
        Card *prevCard;
        insertNameIntoEmptyList(newCard, currentCard, prevCard);

        if (currentCard == headName) {
            newCard->setNextName(currentCard);
            headName = newCard;
        } else {
            newCard->setNextName(currentCard);
            prevCard->setNextName(newCard);
        }
    }

    void CardCollection::insertNameIntoEmptyList(Card *newCard, Card *&currentCard, Card *&prevCard) const {
        currentCard = headName;
        prevCard = 0;
        while (currentCard != 0) {
            if (currentCard->getName() >= newCard->getName()) {
                break;
            } else {
                prevCard = currentCard;
                currentCard = currentCard->getNextName();
            }
        }
    }

    void CardCollection::InsertCardByYear(Card *newCard) {

        // case 1 - Insert in an empty list
        if (this->headYear == 0) {
            this->headYear = newCard;
        } else {
            insertYearAtOrAfterHead(newCard);

        }
        this->size++;
    }

    void CardCollection::insertYearAtOrAfterHead(Card *newCard) {
        Card *currentCard;
        Card *prevCard;
        insertYearIntoEmptyList(newCard, currentCard, prevCard);

        // case 2 - Insert at the head
        if (currentCard == headYear) {
            newCard->setNextYear(currentCard);
            headYear = newCard;
        } else {
            // case 3  - Insert after the head
            newCard->setNextYear(currentCard);
            prevCard->setNextYear(newCard);
        }
    }

    void CardCollection::insertYearIntoEmptyList(Card *newCard, Card *&currentCard, Card *&prevCard) const {
        currentCard = headYear;
        prevCard = 0;
        while (currentCard != 0) {
            if (currentCard->getYear() >= newCard->getYear()) {
                break;
            }
            else {
                prevCard = currentCard;
                currentCard = currentCard->getNextYear();
            }
        }
    }

    void CardCollection::InsertCardByCondition(Card *newCard) {

        if (this->headCondition == 0) {
            this->headCondition = newCard;
        } else {
            insertConditionAtOrAfterHead(newCard);
        }
        this->size++;
    }

    void CardCollection::insertConditionAtOrAfterHead(Card *newCard) {
        Card *currentCard;
        Card *prevCard;
        insertConditionIntoEmptyList(newCard, currentCard, prevCard);

        if (currentCard == headCondition) {
            newCard->setNextCondition(currentCard);
            headCondition = newCard;
        } else {
            newCard->setNextCondition(currentCard);
            prevCard->setNextCondition(newCard);
        }
    }

    void CardCollection::insertConditionIntoEmptyList(Card *newCard, Card *&currentCard, Card *&prevCard) const {
        currentCard = headCondition;
        prevCard = 0;
        while (currentCard != 0) {
            if (currentCard->getCondition() >= newCard->getCondition()) {
                break;
            }
            else {
                prevCard = currentCard;
                currentCard = currentCard->getNextCondition();
            }
        }
    }

    void CardCollection::DeleteCardByName(const string &name) {
        if (this->headName == 0) {
            cout << "The baseball card list is empty." << endl;
        }
        else {
            Card *currentCard;
            Card *prevCard;

            deleteNameFromEmptyList(name, currentCard, prevCard);
            deleteCardNamed(name, currentCard, prevCard);

            DeleteCardByYear(currentCard->getYear());
            DeleteCardByCondition(currentCard->getCondition());

            delete currentCard;
            this->size--;
        }
    }

    void CardCollection::deleteCardNamed(const string &name, Card *currentCard, Card *prevCard) const {
        if (currentCard == 0) {
            cout << "Baseball card with name: " << name << " not found." << endl;
        } else {
            deleteNamedCardFromHead(currentCard, prevCard);
        }
    }

    void CardCollection::deleteNamedCardFromHead(Card *currentCard, Card *prevCard) const {
        if (headName == currentCard) {
            headName->setName(currentCard->getName());
        } else {
            prevCard->setNextName(currentCard->getNextName());
        }
    }

    void CardCollection::deleteNameFromEmptyList(const string &name, Card *&currentCard, Card *&prevCard) const {
        currentCard = headName;
        prevCard = 0;
        while (currentCard != 0) {
            if (CaseConverter::equal_case_insensitive(currentCard->getName(), name)) {
                break;
            }
            else {
                prevCard = currentCard;
                currentCard = currentCard->getNextName();
            }
        }
    }

    void CardCollection::DeleteCardByYear(const int &year) {
        if (this->headYear == 0) {
            cout << "The baseball card list is empty." << endl;
        }
        else {
            Card *currentCard;
            Card *prevCard;

            deleteYearFromEmptyList(year, currentCard, prevCard);
            deleteCardWithYear(currentCard, prevCard);

            this->size--;
        }
    }

    void CardCollection::deleteCardWithYear(Card *currentCard,
                                            Card *prevCard) const {
        if (currentCard == 0) {
            cout << "Baseball card with year: " << currentCard->getYear() << " not found." << endl;
        } else {
            deleteYearFromHead(currentCard, prevCard);

        }
    }

    void CardCollection::deleteYearFromHead(Card *currentCard,
                                            Card *prevCard) const {
        if (headYear == currentCard) {
            headYear->setYear(currentCard->getYear());
        } else {
            prevCard->setNextYear(currentCard->getNextYear());
        }
    }

    void CardCollection::deleteYearFromEmptyList(const int &year, Card *&currentCard, Card *&prevCard) const {
        currentCard = headYear;
        prevCard = 0;
        while (currentCard != 0) {
            if (currentCard->getYear() == year) {
                break;
            }
            else {
                prevCard = currentCard;
                currentCard = currentCard->getNextYear();
            }
        }
    }

    void CardCollection::DeleteCardByCondition(const Conditions &condition) {
        if (this->headCondition == 0) {
            cout << "The baseball card list is empty. " << endl;
        }
        else {
            Card *currentCard;
            Card *prevCard;
            deleteConditionFromEmptyList(condition, currentCard, prevCard);
            deleteConditionedCard(currentCard, prevCard);

            this->size--;
        }
    }

    void CardCollection::deleteConditionedCard(Card *currentCard,
                                               Card *prevCard) const {
        if (currentCard == 0) {
            cout << "Baseball card with condition: " <<
            ConditionConverter::CovertCondition(currentCard->getCondition()) <<
            " not found." <<
            endl;
        } else {
            deleteConditionFromHead(currentCard, prevCard);
        }
    }

    void CardCollection::deleteConditionFromHead(Card *currentCard,
                                                 Card *prevCard) const {
        if (headCondition == currentCard) {
            headCondition->setCondition(currentCard->getCondition());
        } else {
            prevCard->setNextCondition(currentCard->getNextCondition());
        }
    }

    void CardCollection::deleteConditionFromEmptyList(const Conditions &condition, Card *&currentCard,
                                                      Card *&prevCard) const {
        currentCard = headCondition;
        prevCard = 0;
        while (currentCard != 0) {
            if (currentCard->getCondition() == condition) {
                break;
            }
            else {
                prevCard = currentCard;
                currentCard = currentCard->getNextCondition();
            }
        }
    }

    void CardCollection::displayCardsInList(Card *temp) const {
        cout << temp->getName() << setw(10) <<
        temp->getYear() << setw(10) <<
        ConditionConverter::CovertCondition(temp->getCondition()) << setw(10) <<
        "$" << displayAsDollarValue(temp->getValue(), 2) << setw(10) << endl;
    }

    string CardCollection::displayAsDollarValue(const double x, const int decDigits) const {
        stringstream ss;
        ss << fixed;
        ss.precision(decDigits);
        ss << x;
        return ss.str();
    }

    void CardCollection::produceListChronologicalRecursiveAscend(Card *temp) {
        if (temp == 0) {
            return;
        }

        displayCardsInList(temp);
        temp = temp->getNextYear();
        produceListChronologicalRecursiveAscend(temp);
    }

    void CardCollection::produceListChronologicalRecursiveDescend(Card *temp) {
        if (temp == 0) {
            return;
        }

        produceListChronologicalRecursiveDescend(temp->getNextYear());
        displayCardsInList(temp);
    }

    void CardCollection::produceListConditionsRecursiveAscend(Card *temp) {
        if (temp == 0) {
            return;
        }

        displayCardsInList(temp);
        temp = temp->getNextCondition();
        produceListConditionsRecursiveAscend(temp);
    }

    void CardCollection::produceListConditionsRecursiveDescend(Card *temp) {
        if (temp == 0) {
            return;
        }

        produceListConditionsRecursiveDescend(temp->getNextCondition());
        displayCardsInList(temp);
    }

    void CardCollection::printListAlphabeticalAscend() {
        Card *temp = this->GetHeadName();

        if (temp == 0) {
            return;
        }

        displayCardsInList(temp);
        temp = temp->getNextName();
        produceListAlphabeticalRecursiveAscend(temp);
    }

    void CardCollection::printListAlphabeticalDescend() {
        Card *temp = this->GetHeadName();

        if (temp == 0) {
            return;
        }

        produceListAlphabeticalRecursiveDescend(temp->getNextName());
        displayCardsInList(temp);
    }

    void CardCollection::printListChronologicalAscend() {
        Card *temp = this->GetHeadYear();

        if (temp == 0) {
            return;
        }

        displayCardsInList(temp);
        temp = temp->getNextYear();
        produceListChronologicalRecursiveAscend(temp);
    }

    void CardCollection::printListChronologicalDescend() {
        Card *temp = this->GetHeadYear();

        if (temp == 0) {
            return;
        }

        produceListChronologicalRecursiveDescend(temp->getNextYear());
        displayCardsInList(temp);
    }

    void CardCollection::printListConditionsAscend() {
        Card *temp = this->GetHeadCondition();

        if (temp == 0) {
            return;
        }

        displayCardsInList(temp);
        temp = temp->getNextCondition();
        produceListConditionsRecursiveAscend(temp);
    }

    void CardCollection::printListConditionsDescend() {
        Card *temp = this->GetHeadCondition();

        if (temp == 0) {
            return;
        }

        produceListConditionsRecursiveDescend(temp->getNextCondition());
        displayCardsInList(temp);
    }

    void CardCollection::PrintList(CardLaces lace, PrintDirections direction) {
        switch (lace) {
            case CardLaces::NAME:
                if (direction == PrintDirections::ASCENDING) {
                    this->printListAlphabeticalAscend();
                } else if (direction == PrintDirections::DESCENDING) {
                    this->printListAlphabeticalDescend();
                }
                break;
            case CardLaces::YEAR:
                if (direction == PrintDirections::ASCENDING) {
                    this->printListChronologicalAscend();
                } else if (direction == PrintDirections::DESCENDING) {
                    this->printListChronologicalDescend();
                }
                break;
            case CardLaces::CONDITION:
                if (direction == PrintDirections::ASCENDING) {
                    this->printListConditionsAscend();
                } else if (direction == PrintDirections::DESCENDING) {
                    this->printListConditionsDescend();
                }
                break;
        }
    }

    void CardCollection::produceListAlphabeticalRecursiveAscend(Card *temp) {
        if (temp == 0) {
            return;
        }

        displayCardsInList(temp);
        temp = temp->getNextName();
        produceListAlphabeticalRecursiveAscend(temp);
    }

    void CardCollection::produceListAlphabeticalRecursiveDescend(Card *temp) {
        if (temp == 0) {
            return;
        }

        produceListAlphabeticalRecursiveDescend(temp->getNextName());
        displayCardsInList(temp);
    }

    void CardCollection::OutputCardsFromList(string filename) {
        Card *temp = this->headName;

        ofstream outfile;
        outfile.open(filename);

        while (temp != 0) {
            outfile << temp->getName() << "," <<
            temp->getYear() << "," <<
            ConditionConverter::CovertCondition(temp->getCondition()) << "," <<
            temp->getValue() << endl;
            temp = temp->getNextName();
        }
        outfile.close();
    }

    Card *CardCollection::GetHeadName() {
        return this->headName;
    }

    Card *CardCollection::GetHeadYear() const {
        return this->headYear;
    }

    Card *CardCollection::GetHeadCondition() const {
        return this->headCondition;
    }
}
