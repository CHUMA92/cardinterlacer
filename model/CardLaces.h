//
// Created by cokafor3 on 3/27/2016.
//

#ifndef CARDINTERLACER_CARDLACES_H
#define CARDINTERLACER_CARDLACES_H

namespace model {

    /*
     * Card Laces enum class
     */
    enum class CardLaces {
        NAME,
        YEAR,
        CONDITION
    };
}


#endif //CARDINTERLACER_CARDLACES_H
