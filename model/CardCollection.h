//
// Created by cokafor3 on 3/17/2016.
//

#ifndef CARDINTERLACER_CARDCOLLECTION_H
#define CARDINTERLACER_CARDCOLLECTION_H


#include "Card.h"
#include "PrintDirections.h"
#include "CardLaces.h"


namespace model {

    /*
     * Card Collection class
     */
    class CardCollection {
    private:
        Card *headName;
        Card *headYear;
        Card *headCondition;
        int size;

        void DeleteCardByYear(const int &year);

        void DeleteCardByCondition(const Conditions &condition);

        void produceListChronologicalRecursiveAscend(Card *temp);

        void produceListChronologicalRecursiveDescend(Card *temp);

        void produceListConditionsRecursiveAscend(Card *temp);

        void produceListConditionsRecursiveDescend(Card *temp);

        void printListAlphabeticalAscend();

        void printListAlphabeticalDescend();

        void printListChronologicalAscend();

        void printListChronologicalDescend();

        void printListConditionsAscend();

        void printListConditionsDescend();

        void produceListAlphabeticalRecursiveAscend(Card *temp);

        void produceListAlphabeticalRecursiveDescend(Card *temp);

        string displayAsDollarValue(const double x, const int decDigits) const;

        void displayCardsInList(Card *temp) const;

        void deleteNameFromEmptyList(const string &name, Card *&currentCard, Card *&prevCard) const;

        void deleteCardNamed(const string &name, Card *currentCard, Card *prevCard) const;

        void deleteNamedCardFromHead(Card *currentCard, Card *prevCard) const;

        void deleteYearFromEmptyList(const int &year, Card *&currentCard, Card *&prevCard) const;

        void deleteCardWithYear(Card *currentCard, Card *prevCard) const;

        void deleteYearFromHead(Card *currentCard, Card *prevCard) const;

        void deleteConditionFromEmptyList(const Conditions &condition, Card *&currentCard, Card *&prevCard) const;

        void deleteConditionedCard(Card *currentCard, Card *prevCard) const;

        void deleteConditionFromHead(Card *currentCard, Card *prevCard) const;

        void insertNameIntoEmptyList(Card *newCard, Card *&currentCard, Card *&prevCard) const;

        void insertNameAtOrAfterHead(Card *newCard);

        void insertYearIntoEmptyList(Card *newCard, Card *&currentCard, Card *&prevCard) const;

        void insertYearAtOrAfterHead(Card *newCard);

        void insertConditionIntoEmptyList(Card *newCard, Card *&currentCard, Card *&prevCard) const;

        void insertConditionAtOrAfterHead(Card *newCard);

    public:
        /*
         * Default Constructor
         */
        CardCollection();

        /*
         * Deconstructor
         */
        virtual ~CardCollection();

        /*
         * Insert card and arrange by name
         */
        void InsertCardByName(Card *newCard);

        /*
         * Insert card and arrange by year
         */
        void InsertCardByYear(Card *newCard);

        /*
         * Insert card and arrange by year
         */
        void InsertCardByCondition(Card *newCard);

        /*
         * Delete card and arrange by name
         */
        void DeleteCardByName(const string &name);

        /*
         * Head card name getter
         */
        Card *GetHeadName();

        /*
         * Head card year getter
         */
        Card *GetHeadYear() const;

        /*
         * Head card condition getter
         */
        Card *GetHeadCondition() const;

        /*
         * Outputs the card list to the output file
         */
        void OutputCardsFromList(string filename);

        /*
         * Prints the cards in the list
         */
        void PrintList(CardLaces lace, PrintDirections direction);
    };
}

#endif //CARDINTERLACER_CARDCOLLECTION_H
