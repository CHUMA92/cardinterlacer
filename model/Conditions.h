//
// Created by cokafor3 on 3/26/2016.
//

#ifndef CARDINTERLACER_CONDITIONS_H
#define CARDINTERLACER_CONDITIONS_H

namespace model {

    /*
     * Condition enum class
     */
    enum class Conditions {
        POOR,
        GOOD,
        EXCELLENT,
        MINT,
        PRISTINE,
        NONE
    };
}



#endif //CARDINTERLACER_CONDITIONS_H
