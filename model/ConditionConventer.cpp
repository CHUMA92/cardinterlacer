//
// Created by cokafor3 on 3/26/2016.
//

#include <iostream>
#include "ConditionConventer.h"

namespace model {
    string ConditionConverter::CovertCondition(Conditions condition) {
        switch (condition) {
            case Conditions::PRISTINE:
                return "Pristine";
            case Conditions::MINT:
                return "Mint";
            case Conditions::EXCELLENT:
                return "Excellent";
            case Conditions::GOOD:
                return "Good";
            case Conditions::POOR:
                return "Poor";
            default:
                return "Invalid condition type.";
        }
    }

    Conditions ConditionConverter::CovertString(const string &condition) {
        Conditions theCondition;
        if (condition == "Pristine") {
            theCondition = Conditions::PRISTINE;
        } else if (condition == "Mint") {
            theCondition = Conditions::MINT;
        } else if (condition == "Excellent") {
            theCondition = Conditions::EXCELLENT;
        } else if (condition == "Good") {
            theCondition = Conditions::GOOD;
        } else if (condition == "Poor") {
            theCondition = Conditions::POOR;
        } else {
            theCondition = Conditions::NONE;
        }
        return theCondition;
    }

    bool ConditionConverter::CheckIfCondition(const string &condition) {
        return condition == "Pristine" || condition == "Mint" || condition == "Excellent" || condition == "Good" || condition == "Poor";
    }
}

