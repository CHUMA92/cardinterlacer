//
// Created by cokafor3 on 3/26/2016.
//

#ifndef CARDINTERLACER_UTILITY_H
#define CARDINTERLACER_UTILITY_H

#include <string>
#include "Conditions.h"


using namespace std;

namespace model {

    /*
     * Condition converter class
     */
    class ConditionConverter {
    public:
        /*
         * Converts a condition into a string value
         */
        static string CovertCondition(Conditions condition);

        /*
         * Converts a string value into a condition
         */
        static Conditions CovertString(const string &condition);

        /*
         * Checks if string input is a condition
         */
        static bool CheckIfCondition(const string &condition);
    };
}


#endif //CARDINTERLACER_UTILITY_H
