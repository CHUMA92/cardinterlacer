//
// Created by cokafor3 on 3/15/2016.
//

#ifndef CARDINTERLACER_CARDCONTROLLER_H
#define CARDINTERLACER_CARDCONTROLLER_H


#include "TUI.h"
#include "../io/CardFileIO.h"

using namespace io;
using namespace view;

namespace CardController {

    /*
    * Card Controller class
    */
    class CardController {

    private:
        TUI view;
        CardFileIO *cardFile;

        int getAction();

    public:
        /*
         * Constructor
         */
        CardController();

        /*
         * Deconstructor
         */
        virtual ~CardController();

        /*
         * Starts the controller
         */
        int run();
    };
}



#endif //CARDINTERLACER_CARDCONTROLLER_H
