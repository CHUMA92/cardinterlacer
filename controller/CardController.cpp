//
// Created by cokafor3 on 3/15/2016.
//

#include "CardController.h"
#include <iostream>
#include <cstdlib>
#include <limits>

namespace CardController {
    CardController::CardController() {
        this->cardFile = new CardFileIO;
    }

    CardController::~CardController() {
        this->cardFile = 0;
    }

    int CardController::getAction() {
        int choice;
        cin >> choice;
        cout << endl;
        return choice;
    }

    int CardController::run() {
        int choice = 0;
        cout << "Welcome to Chuma Okafor’s baseball card collector." << endl << endl;
        do {
            this->view.displayMainMenu();
            choice = getAction();
            switch (choice) {
                case 1:
                    cardFile->load();
                    break;
                case 2:
                    cardFile->save();
                    break;
                case 3:
                    cardFile->InsertCard();
                    break;
                case 4:
                    cardFile->deleteCard();
                    break;
                case 5:
                    cardFile->printListAlphabeticalAscend();
                    break;
                case 6:
                    cardFile->printListAlphabeticalDescend();
                    break;
                case 7:
                    cardFile->printChronologicalAscend();
                    break;
                case 8:
                    cardFile->printChronologicalDescend();
                    break;
                case 9:
                    cardFile->printStateAscend();
                    break;
                case 10:
                    cardFile->printStateDescend();
                    break;
                case 11:
                    cout << "GOOD-bye.";
                    break;
                default:
                    cin.clear();
                    cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
                    cerr << "Enter a valid action!" << endl;
                    break;
            }
        } while (choice != 11);
        return EXIT_SUCCESS;
    }
}

