//
// Created by cokafor3 on 3/17/2016.
//

#include <ostream>
#include <iostream>
#include <iomanip>
#include <ConditionConventer.h>
#include <sstream>
#include "TUI.h"

namespace view {
    TUI::TUI() {
    }

    TUI::~TUI() {
    }

    void TUI::displayMainMenu() {
        cout<< endl;
        cout << "Please enter the desired action: " << endl << endl;
        cout << "1. Load file" << endl;
        cout << "2. Save file" << endl;
        cout << "3. Insert card" << endl;
        cout << "4. Delete card" << endl;
        cout << "5. Print alphabetic" << endl;
        cout << "6. Reverse alphabetic" << endl;
        cout << "7. Year ascending" << endl;
        cout << "8. Year descending" << endl;
        cout << "9. Condition ascending" << endl;
        cout << "10. Condition descending" << endl;
        cout << "11. Quit" << endl << endl;
        cout << "Enter action> ";
    }



}
