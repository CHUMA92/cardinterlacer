//
// Created by cokafor3 on 3/17/2016.
//

#ifndef CARDINTERLACER_CARDVIEW_H
#define CARDINTERLACER_CARDVIEW_H


#include <CardCollection.h>
#include "Card.h"

using namespace model;

namespace view {

    /*
     * TUI class
     */
    class TUI {
    private:
        CardCollection cards;

    public:
        /*
         * Constructor
         */
        TUI();

        /*
         * Deconstructor
         */
        virtual ~TUI();

        /*
         * Displays the menu
         */
        void displayMainMenu();
    };
}

#endif //CARDINTERLACER_CARDVIEW_H
