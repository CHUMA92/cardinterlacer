cmake_minimum_required(VERSION 3.3)
project(cardInterlacer)

set(CMAKE_RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/bin")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++11 -Wall")

include_directories(controller model view io text)

set(SOURCE_FILES main.cpp io/CardFileIO.cpp io/CardFileIO.h controller/CardController.cpp controller/CardController.h model/Card.cpp model/Card.h model/CardCollection.cpp model/CardCollection.h view/TUI.cpp view/TUI.h model/Conditions.h model/ConditionConventer.cpp model/ConditionConventer.h model/PrintDirections.h model/CardLaces.h model/CaseConverter.cpp model/CaseConverter.h)
add_executable(cardInterlacer ${SOURCE_FILES})