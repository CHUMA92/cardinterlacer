#include <iostream>
#include "CardController.h"

int main(int argc, char *argv[]) {
    CardController::CardController controller;
    controller.run();

    return 0;
}